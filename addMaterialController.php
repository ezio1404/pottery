<?php
include 'dbhelper.php';
if(isset($_POST['btnAddMatToPot'])){
    $pot_id=$_GET['id'];
    $mat_id=$_POST['materials'];
    $consumption=$_POST['consumption'];

    $data = array($pot_id,$mat_id,$consumption);

    $matinpotList=getAllmatinPot();
    $flag = true;
    foreach($matinpotList as $mp){
        // echo $mp['mat_id'];
        if($mp['mat_id'] == $mat_id && $mp['pot_id']==$pot_id){
           $flag = false;
        }
    }
    if($consumption <= 0){
        header("location:addPotMaterial.php?id=".$pot_id."&message=error");
    }else{
    if($flag){
        addRawMaterialToPot($data);
        header("location:addPotMaterial.php?id=".$pot_id."&message=success");
    }else{
        header("location:addPotMaterial.php?id=".$pot_id."&message=duplicate");
    }
}
    // die;
    
}