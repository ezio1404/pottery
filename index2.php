<?php
include 'dbhelper.php';
?>
<!DOCTYPE html>
<html class="no-js">
<head>
	<title>CandyCar - Repair, Towing, Tuning SinglePage and MultePage HTML template</title>
	<meta charset="utf-8">
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/animations.css">
	<link rel="stylesheet" href="css/font-awesome.css">
	<link rel="stylesheet" href="css/main.css" class="color-switcher-link">
	<link rel="stylesheet" href="css/shop.css" class="color-switcher-link">
	<script src="js/vendor/modernizr-2.6.2.min.js"></script>



</head>

<body>

	<div class="preloader">
		<div class="preloader_image"></div>
	</div>
	<div class="container ">
	<a href="rawMaterials.php" class="btn btn-small btn-maincolor">Add Raw Materials</a>
	<a href="pots.php" class="btn btn-small btn-maincolor">Add Pots</a>
	</div>



	<ul id="products" class="products list-unstyled grid-view">
		<li class="product vertical-item padding-small content-padding">
			<div class="product-inner bordered">
				<a class="link-scale" href="rimpot.php">
					<img src="images/pots/WhiteWashedTerraCotta.png" alt="">
				</a>
				<div class="item-content">
					<h2>8" dia Scalloped Rim Pot</h2>
					<div class=""></div>
					<span style="width:80%">Measurement: <strong class="rating">8" x 8" x 5.75" HT</strong> </span>
				</div>
				<a href="rimpot.php" class="btn btn-small btn-maincolor">VIEW ITEM</a>
			</div>
			<br>
			</div>
		</li>
		<!-- POTS -->
		<li class="product vertical-item padding-small content-padding">
			<div class="product-inner bordered">
				<a class="link-scale" href="weatheredpot.php">
					<img src="images/pots/weaheredBrown.png" alt="">
				</a>
				<div class="item-content">
					<h2>14.69" Square Faux Wood Planter <br> Weathered Brown </h2>
					<div class=""></div>
					<span style="width:80%">Measurement: <strong class="rating">16"Sq x 13.75"HT</strong> </span>
				</div>
				<a href="weatheredpot.php" class="btn btn-small btn-maincolor">VIEW ITEM</a>
			</div>
			<br>
			</div>
		</li>
		<!-- POTS -->
		<li class="product vertical-item padding-small content-padding">
			<div class="product-inner bordered">
				<a class="link-scale" href="texturepot.php">
					<img src="images/pots/ErodedTexture.png" alt="">
				</a>
				<div class="item-content">
					<h2>13.75" dia Eroded Texture Pot</h2>
					<div class=""></div>
					<span style="width:80%">Measurement: <strong class="rating">15,25" dia x 12"HT</strong> </span>
				</div>
				<a href="texturepot.php" class="btn btn-small btn-maincolor">VIEW ITEM</a>
			</div>
			<br>
			</div>
		</li>
		<!-- POTS -->
		<li class="product vertical-item padding-small content-padding">
			<div class="product-inner bordered">
				<a class="link-scale" href="footedpot.php">
					<img src="images/pots/FootedPo.png" alt="">
				</a>
				<div class="item-content">
					<h2>10.5" dia Footed Pot</h2>
					<div class=""></div>
					<span style="width:80%">Measurement: <strong class="rating">12" dia x 8" HT</strong> </span>
				</div>
				<a href="footedpot.php" class="btn btn-small btn-maincolor">VIEW ITEM</a>
			</div>
			<br>
			</div>
		</li>
		<!-- POTS -->
		<li class="product vertical-item padding-small content-padding">
			<div class="product-inner bordered">
				<a class="link-scale" href="weevestool.php">
					<img src="images/pots/Herringbone.png" alt="">
				</a>
				<div class="item-content">
					<h2>18.5" H Herringbone Weave Stool</h2>
					<div class=""></div>
					<span style="width:80%">Measurement: <strong class="rating">13.25" dia x 18.5" HT</strong> </span>
				</div>
				<a href="weevestool.php" class="btn btn-small btn-maincolor">VIEW ITEM</a>
			</div>
			<br>
			</div>
		</li>
		<!-- POTS -->
		<li class="product vertical-item padding-small content-padding">
			<div class="product-inner bordered">
				<a class="link-scale" href="weavepot.php">
					<img src="images/pots/naturalWoven.png" alt="">
				</a>
				<div class="item-content">
					<h2>12.75" dia Herringbone Weave Pot</h2>
					<div class=""></div>
					<span style="width:80%">Measurement: <strong class="rating">13.25" dia x 18.5" HT</strong> </span>
				</div>
				<a href="weavepot.php" class="btn btn-small btn-maincolor">VIEW ITEM</a>
			</div>
			<br>
			</div>
		</li>

		<!-- POTS -->
		<li class="product vertical-item padding-small content-padding">
			<div class="product-inner bordered">
				<a class="link-scale" href="texturepot.php">
					<img src="images/pots/ErodedTexture.png" alt="">
				</a>
				<div class="item-content">
					<h2>13.75" dia Eroded Texture Pot</h2>
					<div class=""></div>
					<span style="width:80%">Measurement: <strong class="rating">15,25" dia x 12"HT</strong> </span>
				</div>
				<a href="texturepot.php" class="btn btn-small btn-maincolor">VIEW ITEM</a>
			</div>
			<br>
			</div>
		</li>

		<ul>



			<script src="js/compressed.js"></script>
			<script src="js/main.js"></script>
			<!-- <script src="js/switcher.js"></script> -->

</body>

</html>