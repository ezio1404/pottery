<?php
include 'dbhelper.php';
$materialList = getAllMaterials();

?>
<!DOCTYPE html>
<html class="no-js">

<head>
    <title>Pottery</title>
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/animations.css">
    <link rel="stylesheet" href="css/font-awesome.css">
    <link rel="stylesheet" href="css/main.css" class="color-switcher-link">
    <link rel="stylesheet" href="css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="css/buttons.dataTables.min.css">
    <script src="js/vendor/modernizr-2.6.2.min.js"></script>



</head>

<body>


    <div class="preloader">
        <div class="preloader_image"></div>
    </div>



    <!-- wrappers for visual page editor and boxed version of template -->
    <div id="canvas">
        <div id="box_wrapper">

            <!-- template sections -->


            <div class="header_absolute s-parallax ds bs s-overlay">

                <!--topline section visible only on small screens|-->
                <section class="page_toplogo ds s-overlay s-pt-10 s-pb-5 s-py-lg-30">
                    <div class="container">
                        <div class="row align-items-center">
                            <div class="col-lg-12">
                                <div class="d-lg-flex justify-content-lg-end align-items-lg-center">
                                    <div class="mr-auto">
                                        <!--
							if you want to display toplogo info on smaller screens
							than use following CSS classes below:
						 	d-sm-flex justify-content-sm-center
						 -->
                                        <div class="d-none d-lg-flex justify-content-center justify-content-lg-start">
                                            <a href="index.php" class="logo">
                                                <img src="images/logo.png" alt="">
                                                <span class="logo-text fw-500">Cebu<span
                                                        class="fw-200">Pottery</span></span>
                                            </a>
                                        </div>
                                    </div>
                                    <!--
						if you want to display toplogo info on smaller screens
						than use following CSS classes below:
						d-sm-flex justify-lg-content-end justify-content-sm-between align-items-center
					-->
                                    <div class="d-flex justify-lg-content-end align-items-center meta-icons">


                                    </div>
                                </div>
                                <!-- header toggler -->
                            </div>
                        </div>
                    </div>
                </section>


                <!--eof topline-->

                <!-- header with single Bootstrap column only for navigation and includes. Used with topline and toplogo sections. Menu toggler must be in toplogo section -->
                <header class="page_header s-py-10 s-py-lg-0 ds ms s-overlay nav-bordered justify-nav-center">
                    <div class="container-fluid">
                        <div class="row align-items-center">
                            <div class="d-lg-none col-11">
                                <a href="index.php" class="logo">
                                    <img src="images/logo.png" alt="">
                                    <span class="logo-text fw-500">Candy<span class="fw-200">Car</span></span>
                                </a>
                            </div>
                            <div class="col-xl-12">

                                <div class="nav-wrap">
                                    <!-- main nav start -->
                                    <nav class="top-nav">
                                        <ul class="nav sf-menu">
                                            <li class=""><a href="index.php">Home</a></li>
                                            <li class="active"><a href="rawMaterials.php">Materials</a></li>
                                            <li class=""><a href="pots.php">Pots</a></li>

                                        </ul>
                                    </nav>
                                    <!-- eof main nav -->


                                </div>

                            </div>
                        </div>
                    </div>

                    <!-- header toggler -->

                    <span class="toggle_menu"><span></span></span>

                </header>

                <section class="page_title ds s-pt-105 s-pb-50 s-pt-lg-115 s-pb-lg-60">
                    <div class="divider-3 d-none d-lg-block"></div>
                    <div class="container">
                        <div class="row">

                            <div class="col-md-12">
                                <h1 class="bold text-center text-lg-left">Add Material</h1>

                            </div>

                        </div>
                    </div>
                </section>


            </div>


            <section class="ls s-pt-50 s-pb-60 s-pt-lg-90 s-pb-lg-100 s-pt-xl-140 s-pb-xl-150">
                <div class="container" style="padding-top:80px !important">
                    <div class="row">
                        <div class="col-lg-4">
                            <?php
                            if($_GET['message']=="success"){
                                ?>
                                <div class="alert alert-success" role="alert">
                                    Success Addding Material
                                </div>
                                <?php
                            }if($_GET['message']=="success_addingstock"){
                            ?>
                            <div class="alert alert-success" role="alert">
                                Success Addding Stock
                             </div>
                            <?php
                            }
                            ?>
                            <form action="materialController.php" method="POST">
                                <div class="container">
                                    <input type="text" name="mat_name" id="mat_name" placeholder="Raw Material Name">
                                </div>
                                <div class="container">
                                    <input type="number" step=".01" name="mat_QoH" id="mat_QoH"
                                        placeholder="Quantity On hand(Kilos)">
                                </div>
                                <div class="container">
                                    <input class="btn btn-outline-success" type="submit" value="Add Material"
                                        name="btnAddMaterial" id="btnAddMaterial">
                                </div>
                            </form>

                        </div>
                        <div class="col-lg-8">
                            <table id="example" class="display nowrap" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Material Name</th>
                                        <th>Quantity on Hand(Kilos)</th>
                                        <th>Level Order</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody id="myBody">
                                    <?php
                                    foreach($materialList as $materialData){
                                    ?>
                                    <tr  style="color:<?php echo $materialData['mat_QoH']<=$materialData['mat_LO']?'red':'';?>;">
                                        <td><?php echo $materialData['mat_id']?></td>
                                        <td><?php echo $materialData['mat_name']?></td>
                                        <td><?php echo $materialData['mat_QoH']?></td>
                                        <td><?php echo $materialData['mat_LO']?></td>
                                        <td><?php echo $materialData['mat_status']?></td>
                                        <td><a class="btn btn-outline-warning btn-sm" href="addStock.php?id=<?php echo $materialData['mat_id']?>">Add Stock</a></td>
                                    </tr>
                                    <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </section>




            <section class="page_copyright ds ms s-pt-5 s-pb-25 s-py-lg-20">
                <div class="container">
                    <div class="divider-2 d-none d-lg-block"></div>
                    <div class="row align-items-center">
                        <div class="divider-20 d-none d-lg-block"></div>

                        <div class="col-md-12 text-center">
                            <p class="social-icons with-border">
                                <span><a href="https://www.facebook.com/maryjoy.guglino"
                                        class="fa fa-facebook border-icon rounded-icon" title="facebook"></a></span>
                                <span><a href="https://github.com/ezio1404"
                                        class="fa fa-github border-icon rounded-icon" title="telegram"></a></span>
                                <span><a href="https://www.instagram.com/"
                                        class="fa fa-instagram border-icon rounded-icon" title="instagram"></a></span>
                            </p>
                        </div>
                        <div class="divider-20 d-none d-lg-block"></div>
                    </div>
                </div>
            </section>


        </div><!-- eof #box_wrapper -->
    </div><!-- eof #canvas -->


    <script src="js/compressed.js"></script>
    <script src="js/main.js"></script>
    <!-- <script src="js/jquery-3.3.1.js"></script> -->
    <script src="js/jquery.dataTables.min.js"></script>
    <script src="js/dataTables.buttons.min.js"></script>
    <script src="js/buttons.print.min.js"></script>
    <script src="js/buttons.flash.min.js"></script>
    <script src="js/buttons.html5.min.js"></script>
    <script src="js/jszip.min.js"></script>
    <script src="js/pdfmake.min.js"></script>
    <script src="js/vfs_fonts.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#example').DataTable({
                "pageLength": 20,
                dom: 'Bfrtip',
                buttons: ['copy', 'csv', 'excel', 'pdf', 'print']
            });
        });
    </script>
</body>

</html>