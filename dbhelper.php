<?php

function dbconn(){
    try{
        return new PDO("mysql:hostname=localhost;dbname=pots_db","root","");
    }catch(PDOExecption $e){
        echo $e->getMessage();
    } 
}//end dbconn()

function destroy(){
    return null;
}//end of destroy()


function addRawMaterial($data){
    $db=dbconn();
    $sql="INSERT INTO tbl_materials(mat_name,mat_QoH,mat_LO) values(?,?,?)";
    $stmt=$db->prepare($sql);
    $stmt->execute($data);
    $db=destroy();  
}// end of addRawMaterial

function addRawMaterialToPot($data){
    $db=dbconn();
    $sql="INSERT INTO tbl_matinpot(pot_id,mat_id,consumption) values(?,?,?)";
    $stmt=$db->prepare($sql);
    $stmt->execute($data);
    $db=destroy();  
}// end of addRawMaterialToPot

function addPot($data){
    $db=dbconn();
    $sql="INSERT INTO tbl_pots(pot_image,pot_desc,pot_measurement,pot_product,pot_PW,pot_color) values(?,?,?,?,?,?)";
    $stmt=$db->prepare($sql);
    $stmt->execute($data);
    $db=destroy();  
}// end of addPot

function getAllMaterials(){
    $dbconn=dbconn();
    $sql="SELECT * FROM tbl_materials";
    $stmt=$dbconn->prepare($sql);
    $stmt->execute();
    $rows=$stmt->fetchAll(PDO::FETCH_ASSOC);
    $dbconn=destroy();
    return $rows;
}// end of getAllMaterials

function getMaterial($data){
    $dbconn=dbconn();
    $sql="SELECT * FROM tbl_materials WHERE mat_id = ?";
    $stmt=$dbconn->prepare($sql);
    $stmt->execute($data);
    $rows=$stmt->fetch(PDO::FETCH_ASSOC);
    $dbconn=destroy();
    return $rows;
}// end of getMaterial

function getAllMaterialsInPot($data){
    $dbconn=dbconn();
    $sql="SELECT * FROM tbl_materials m,tbl_pots p ,tbl_matinpot mp WHERE m.mat_id=mp.mat_id AND p.pot_id=mp.pot_id AND p.pot_id = ?";
    $stmt=$dbconn->prepare($sql);
    $stmt->execute($data);
    $rows=$stmt->fetchAll(PDO::FETCH_ASSOC);
    $dbconn=destroy();
    return $rows;
}// end of getAllMaterialsInPot

function getAllPots(){
    $dbconn=dbconn();
    $sql="SELECT * FROM tbl_pots";
    $stmt=$dbconn->prepare($sql);
    $stmt->execute();
    $rows=$stmt->fetchAll(PDO::FETCH_ASSOC);
    $dbconn=destroy();
    return $rows;
}// end of getAllPots

function getAllmatinPot(){
    $dbconn=dbconn();
    $sql="SELECT * FROM  tbl_matinpot ";
    $stmt=$dbconn->prepare($sql);
    $stmt->execute();
    $rows=$stmt->fetchAll(PDO::FETCH_ASSOC);
    $dbconn=destroy();
    return $rows;
}// end of getAllmatinPot

function getPot($data){
    $dbconn=dbconn();
    $sql="SELECT * FROM tbl_pots WHERE pot_id=?";
    $stmt=$dbconn->prepare($sql);
    $stmt->execute($data);
    $rows=$stmt->fetch(PDO::FETCH_ASSOC);
    $dbconn=destroy();
    return $rows;
}// end of getPot

function updatePot($data){
    $db=dbconn();
    $sql="UPDATE tbl_pots SET pot_image = ? ,pot_desc=?,pot_measurement=?,pot_product=?,pot_PW=?,pot_color=? WHERE pot_id=?";
    $stmt=$db->prepare($sql);
    $stmt->execute($data);
    $db=destroy(); 
}// end of getPot

function createPot($data){
    $db=dbconn();
    $sql="UPDATE tbl_materials SET mat_QoH=? WHERE mat_id=?";
    $stmt=$db->prepare($sql);
    $stmt->execute($data);
    $db=destroy(); 
}// end of createPot

function addStock($data){
    $db=dbconn();
    $sql="UPDATE tbl_materials SET mat_QoH=? WHERE mat_id=?";
    $stmt=$db->prepare($sql);
    $stmt->execute($data);
    $db=destroy(); 
}// end of addStock

function addQuantity($data){
    $db=dbconn();
    $sql="UPDATE tbl_pots SET pot_QoH=pot_QoH+1 WHERE pot_id=?";
    $stmt=$db->prepare($sql);
    $stmt->execute($data);
    $db=destroy(); 
}// end of createPot


