-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 24, 2019 at 09:02 PM
-- Server version: 10.1.32-MariaDB
-- PHP Version: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pots_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_materials`
--

CREATE TABLE `tbl_materials` (
  `mat_id` int(11) NOT NULL,
  `mat_name` varchar(255) NOT NULL,
  `mat_QoH` decimal(10,2) NOT NULL,
  `mat_LO` decimal(10,2) NOT NULL,
  `mat_status` varchar(255) NOT NULL DEFAULT 'On stock',
  `mat_dateCreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_materials`
--

INSERT INTO `tbl_materials` (`mat_id`, `mat_name`, `mat_QoH`, `mat_LO`, `mat_status`, `mat_dateCreated`) VALUES
(1, 'jghkl', '14.00', '5.00', 'On stock', '2019-11-24 20:02:01'),
(2, 'tbsd', '0.30', '12.00', 'On stock', '2019-11-24 19:33:55'),
(3, 'BASD', '12.00', '3.00', 'On stock', '2019-11-24 18:29:49'),
(4, 'bvnm', '9.12', '2.00', 'On stock', '2019-11-24 19:32:48'),
(5, 'teracotaa', '6.42', '2.48', 'On stock', '2019-11-24 19:32:48'),
(6, 'teracotaa123', '21.00', '4.20', 'On stock', '2019-11-24 18:31:20');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_matinpot`
--

CREATE TABLE `tbl_matinpot` (
  `matinpot_id` int(11) NOT NULL,
  `pot_id` int(11) NOT NULL,
  `mat_id` int(11) NOT NULL,
  `consumption` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_matinpot`
--

INSERT INTO `tbl_matinpot` (`matinpot_id`, `pot_id`, `mat_id`, `consumption`) VALUES
(1, 123123130, 4, '0.50'),
(25, 123123131, 2, '0.70'),
(28, 123123130, 5, '1.00'),
(29, 123123130, 2, '0.42'),
(30, 123123130, 6, '0.30');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pots`
--

CREATE TABLE `tbl_pots` (
  `pot_id` int(11) NOT NULL,
  `pot_image` varchar(255) NOT NULL,
  `pot_desc` varchar(255) NOT NULL,
  `pot_measurement` varchar(255) NOT NULL,
  `pot_product` varchar(255) NOT NULL,
  `pot_PW` varchar(255) NOT NULL,
  `pot_color` varchar(255) NOT NULL,
  `pot_QoH` int(11) NOT NULL DEFAULT '0',
  `pot_dateCreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_pots`
--

INSERT INTO `tbl_pots` (`pot_id`, `pot_image`, `pot_desc`, `pot_measurement`, `pot_product`, `pot_PW`, `pot_color`, `pot_QoH`, `pot_dateCreated`) VALUES
(123123130, '2646-What-To-Do-With-Lemons.jpg', 'eqw', 'ewqeqwe', 'ewqeqw', 'ewqe', 'ewqeqw', 7, '2019-11-24 19:32:48'),
(123123131, '7612-51bcBpTG28L._SX425_.jpg', 'orange pot', '10 x 20', 'orangeeee', '20 kilo', 'red', 2, '2019-11-24 19:33:55');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_materials`
--
ALTER TABLE `tbl_materials`
  ADD PRIMARY KEY (`mat_id`);

--
-- Indexes for table `tbl_matinpot`
--
ALTER TABLE `tbl_matinpot`
  ADD PRIMARY KEY (`matinpot_id`);

--
-- Indexes for table `tbl_pots`
--
ALTER TABLE `tbl_pots`
  ADD PRIMARY KEY (`pot_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_materials`
--
ALTER TABLE `tbl_materials`
  MODIFY `mat_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tbl_matinpot`
--
ALTER TABLE `tbl_matinpot`
  MODIFY `matinpot_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `tbl_pots`
--
ALTER TABLE `tbl_pots`
  MODIFY `pot_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=123123132;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
