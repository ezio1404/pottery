<?php
include 'dbhelper.php';
$potData = getPot(array($_GET['id']));

?>
<!DOCTYPE html>
<html class="no-js">

<head>
    <title>Pottery</title>
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/animations.css">
    <link rel="stylesheet" href="css/font-awesome.css">
    <link rel="stylesheet" href="css/main.css" class="color-switcher-link">
    <script src="js/vendor/modernizr-2.6.2.min.js"></script>



</head>

<body>


    <div class="preloader">
        <div class="preloader_image"></div>
    </div>



    <!-- wrappers for visual page editor and boxed version of template -->
    <div id="canvas">
        <div id="box_wrapper">

            <!-- template sections -->


            <div class="header_absolute s-parallax ds bs s-overlay">

                <!--topline section visible only on small screens|-->
                <section class="page_toplogo ds s-overlay s-pt-10 s-pb-5 s-py-lg-30">
                    <div class="container">
                        <div class="row align-items-center">
                            <div class="col-lg-12">
                                <div class="d-lg-flex justify-content-lg-end align-items-lg-center">
                                    <div class="mr-auto">
                                        <!--
							if you want to display toplogo info on smaller screens
							than use following CSS classes below:
						 	d-sm-flex justify-content-sm-center
						 -->
                                        <div class="d-none d-lg-flex justify-content-center justify-content-lg-start">
                                            <a href="index.php" class="logo">
                                                <img src="images/logo.png" alt="">
                                                <span class="logo-text fw-500">Cebu<span
                                                        class="fw-200">Pottery</span></span>
                                            </a>
                                        </div>
                                    </div>
                                    <!--
						if you want to display toplogo info on smaller screens
						than use following CSS classes below:
						d-sm-flex justify-lg-content-end justify-content-sm-between align-items-center
					-->
                                    <div class="d-flex justify-lg-content-end align-items-center meta-icons">


                                    </div>
                                </div>
                                <!-- header toggler -->
                            </div>
                        </div>
                    </div>
                </section>


                <!--eof topline-->

                <!-- header with single Bootstrap column only for navigation and includes. Used with topline and toplogo sections. Menu toggler must be in toplogo section -->
                <header class="page_header s-py-10 s-py-lg-0 ds ms s-overlay nav-bordered justify-nav-center">
                    <div class="container-fluid">
                        <div class="row align-items-center">
                            <div class="d-lg-none col-11">
                                <a href="index.php" class="logo">
                                    <img src="images/logo.png" alt="">
                                    <span class="logo-text fw-500">Candy<span class="fw-200">Car</span></span>
                                </a>
                            </div>
                            <div class="col-xl-12">

                                <div class="nav-wrap">
                                    <!-- main nav start -->
                                    <nav class="top-nav">
                                        <ul class="nav sf-menu">
                                            <li class=""><a href="index.php">Home</a></li>
                                            <li class=""><a href="rawMaterials.php">Materials</a></li>
                                            <li class="active"><a href="pots.php">Pots</a></li>

                                        </ul>
                                    </nav>
                                    <!-- eof main nav -->


                                </div>

                            </div>
                        </div>
                    </div>

                    <!-- header toggler -->

                    <span class="toggle_menu"><span></span></span>

                </header>

                <section class="page_title ds s-pt-105 s-pb-50 s-pt-lg-115 s-pb-lg-60">
                    <div class="divider-3 d-none d-lg-block"></div>
                    <div class="container">
                        <div class="row">

                            <div class="col-md-12">
                                <h1 class="bold text-center text-lg-left">Add Pot</h1>

                            </div>

                        </div>
                    </div>
                </section>


            </div>


            <section class="ls s-pt-50 s-pb-60 s-pt-lg-90 s-pb-lg-100 s-pt-xl-140 s-pb-xl-150">
                <div class="container" style="padding-top:80px !important">
                    <div class="row">
                        <div class="col-lg-8">
                            <form action="potController.php?id=<?php echo $potData['pot_id'];?>" method="POST" enctype="multipart/form-data">
                                <div class="container">
                                    <!-- <input type="file" name="file" id="pot_image" placeholder="pot_image" value="<?php echo $potData['pot_image'];?>"> -->
                                    <img id="blah" src="photo/<?php echo $potData['pot_image'];?>" style="height : 300px !important; width : auto !important;  margin-left: auto;
                                    margin-right: auto;"  />
                                </div>
                                <div class="container">
                                    <input type="text" disabled name="pot_desc" id="pot_desc" placeholder="Pot DESC" value="<?php echo $potData['pot_desc'];?>">
                                </div>
                                <div class="container">
                                    <input type="text" disabled name="pot_measurement" id="pot_measurement" 
                                        placeholder="pot_measurement" value="<?php echo $potData['pot_measurement'];?>">
                                </div>
                                <div class="container"> <input type="text" disabled name="pot_product" id="pot_product" value="<?php echo $potData['pot_product'];?>"
                                        placeholder="pot_product"></div>
                                <div class="container"> <input type="text" disabled name="pot_PW" id="pot_PW" value="<?php echo $potData['pot_PW'];?>"
                                        placeholder="pot_PW"></div>
                                <div class="container"><input type="text" disabled name="pot_color" id="pot_color" value="<?php echo $potData['pot_color'];?>"
                                        placeholder="pot_color"></div>
                                <input class="btn btn-outline-success" type="submit" value="Update Pot" name="btnUpdatePot" id="btnUpdatePot">
                            </form>    
                        </div>
                       
                    </div>
                </div>
            </section>




            <section class="page_copyright ds ms s-pt-5 s-pb-25 s-py-lg-20">
                <div class="container">
                    <div class="divider-2 d-none d-lg-block"></div>
                    <div class="row align-items-center">
                        <div class="divider-20 d-none d-lg-block"></div>

                        <div class="col-md-12 text-center">
                            <p class="social-icons with-border">
                                <span><a href="https://www.facebook.com/maryjoy.guglino"
                                        class="fa fa-facebook border-icon rounded-icon" title="facebook"></a></span>
                                <span><a href="https://github.com/ezio1404"
                                        class="fa fa-github border-icon rounded-icon" title="telegram"></a></span>
                                <span><a href="https://www.instagram.com/"
                                        class="fa fa-instagram border-icon rounded-icon" title="instagram"></a></span>
                            </p>
                        </div>
                        <div class="divider-20 d-none d-lg-block"></div>
                    </div>
                </div>
            </section>


        </div><!-- eof #box_wrapper -->
    </div><!-- eof #canvas -->


    <script src="js/compressed.js"></script>
    <script src="js/main.js"></script>
    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#pot_image").change(function () {
            readURL(this);
        });
    </script>
</body>

</html>