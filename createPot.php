<?php
include 'dbhelper.php';

$materialList = getAllMaterials();
$matinpotList = getAllMaterialsInPot(array($_GET['id']));
$flag = true;
$potFlag=false;
foreach($materialList as $matData){
    foreach($matinpotList as $mp){
        if($matData['mat_id']==$mp['mat_id']){
            if($matData['mat_QoH']<$mp['consumption']){
                $flag=false;
                header("location:addPotMaterial.php?id=".$_GET['id']."&message=error_creatingPot");
            }else{
                // echo $matData['mat_QoH']."--".$mp['consumption']."<br>";
            }
        }
    }
}

if($flag){
    foreach($materialList as $matData){
        foreach($matinpotList as $mp){
            if($matData['mat_id']==$mp['mat_id']){
                $diff = $matData['mat_QoH']-$mp['consumption'];
                createPot(array($diff ,$matData['mat_id']));
                $potFlag=true;
            }
        }
    }
}
if($potFlag){
    addQuantity(array($_GET['id']));
    header("location:addPotMaterial.php?id=".$_GET['id']."&message=success_creatingPot");
}

